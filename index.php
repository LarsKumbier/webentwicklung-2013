<?php
require_once('lib/tools.php');
require_once('lib/bcrypt.php');
require_once('lib/FirePHPCore/fb.php');


/**
 * Lädt Konfigurationsdaten
 */
require_once('config/config.php.global');
@include_once('config/config.php.local');

$basePath = getBasePath($_SERVER['PHP_SELF']);
$pathInfo = getPathInfo($_SERVER['REQUEST_URI'], $basePath);


/**
 * Starte Sessions
 */
$user = null;
if (!session_start())
	returnHttp500('konnte Sessions nicht starten');

if (array_key_exists('username', $_SESSION)) {
	$user = array (
		'username' => $_SESSION['username'],
		'userRole' => $_SESSION['userRole'],
		'userFirstname' => $_SESSION['userFirstname'],
		'userLastname' => $_SESSION['userLastname']
		);
}


/**
 * Sind alle notwendigen Erweiterungen von PHP geladen?
 */
if (!function_exists('mysql_connect')) {
	returnHttp500('The Mysql-Extension is not loaded.');
}



/**
 * Baue die Datenbankverbindung auf
 */
if ($config['db']['database'] == null ||
    $config['db']['username'] == null ) {
	returnHttp500('Username or Database have not been set in config/config.php.local');
}

$dbCon = mysql_connect(
	$config['db']['server'].':'.$config['db']['port'],
	$config['db']['username'],
	$config['db']['password']);

if (!$dbCon) {
	returnHttp500('Mysql-Error - Connecting to server ('.mysql_errno().'): '.mysql_error());
}

if (!mysql_select_db($config['db']['database'], $dbCon)) {
	returnHttp500('Mysql-Error - Selecting database ('.mysql_errno().'): '.mysql_error());
}


// Prüfe, ob eine Unterseite aufgerufen wurde
if ($pathInfo == null) {
	$pathInfo = array(
		$config['route']['defaultController'],
		$config['route']['defaultAction']);
}


// Aufzurufenden Controller bestimmen
$controller = strtolower($pathInfo[0]);
$controllerPath = 'modules/'.$controller.'/';
$controllerName = ucfirst($controller).'Controller';
$controllerPath .= $controllerName.'.php';


// Aufzurufende Action bestimmen
if (empty($pathInfo[1])) {
	$action = $config['route']['defaultAction'];
} else {
	$action = strtolower($pathInfo[1]);
}

$action = ucfirst($action).'Action';


// Prüfe, ob ein existierender Pfad angegeben wurde
if (!file_exists($controllerPath)) {
	returnHttp404();
}

// Lade die ControllerDatei ein, damit wir auf die Klasse zugreifen 
// können und instantiiere ein Objekt der ControllerKlasse
require_once($controllerPath);
$controller = new $controllerName;

// Prüfe, ob ein existierender Pfad angegeben wurde
if (!method_exists($controller, $action)) {
	returnHttp404();
}

// Übergib die Kontrolle an den jeweiligen Controller
$controller->$action();
