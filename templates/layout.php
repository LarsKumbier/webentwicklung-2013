<!DOCTYPE html>
<html>
	<head>
		<title>Barney Stinson - <?php echo $viewContent['title']; ?></title>
		<base href="<?php echo getBasePath($_SERVER['PHP_SELF']); ?>">
		<link href="_style/reset.css"
		      rel="stylesheet"
		      type="text/css" />
		<link href="_style/global.php"
		      rel="stylesheet"
		      type="text/css" />
		<meta charset="UTF-8">
	</head>
	<body>
		<div id="container" class="centered">
			<div id="header">
				<img src="_images/logo.jpg" 
				     class="logo" 
				     alt="Logo meiner Homepage: Barny Stinson True Story"
				     title="Logo meiner Homepage: Barny Stinson True Story">
				<h1>Barny Stinson</h1>
				<h2>legen... wait for it! ...dary True Stories</h2>
			</div>
			
			<div id="navigation">
				<ul>
					<li><a href="home"><?php t('Home'); ?></a></li>
					<li><a href="news"><?php t('News'); ?></a></li>
					<li><a href="brocode"><?php t('The BroCode'); ?></a></li>
					<li><a href="meet"><?php t('Meet Barney'); ?></a></li>
					<li><a href="shop"><?php t('BarneyWare'); ?></a></li>
				</ul>
				<?php if (array_key_exists('username', $_SESSION)) : ?>
				<hr>
				<p>
					Meister <?=$_SESSION['userFirstname']?>, welcome back!
				</p>
				<ul>
					<li><a href="user/logout">Logout</a></li>
				</ul>
				<?php endif; ?>
			</div>
			
			<div id="content">
				<?php if (array_key_exists('msg', $_SESSION)) : ?>
				<div id="message">
					<?=$_SESSION['msg']?>
					<?php unset($_SESSION['msg']); ?>
				</div>
				<?php endif; ?>
				<?php require ($viewContent['template']); ?>
			</div> <!-- end of #content -->
			
			<div id="footer">
				<span style="color: #000">
					<a href="user/login">Admin</a>
				</span>
				<span class="footerContent">
					Created By <a href="http://kumbier.it">Lars Kumbier</a>
				</span>
			</div>
		</div> <!-- end of container -->
	</body>
</html>
