<?php

$translations = array (
	'Home' => array ('de' => 'Start', 
	                 'id' => "s'is halt der Beginn",
	                 'de-at' => "Hoimat"),
	'News' => array ('de' => 'Neues', 
	                 'id' => "Schwätz ma was Neues",
	                 'de-at' => "Noies"),
	'The BroCode' => array ('de' => 'Der Codex der Bruderschaft', 
	                        'id' => "Wi kum I da Dirndl?"),
	'_LOREM_' => array ('de' => 'Lorem Ipsum Dolor Sit Amet. Lorem Ipsum Dolor Sit Amet. Lorem Ipsum Dolor Sit Amet.')
	);



/**
 * vorbereitende Funktion für Übersetzungen
 */
function t($string)
{
	global $translations;
	
	$acceptLang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	
	$langParts = explode(',', $acceptLang);
	foreach ($langParts as $key => $lang)
		$langParts[$key] = current(explode(';', $lang));
	
	reset($langParts);
	if (array_key_exists($string, $translations)) {
		foreach ($langParts as $lang) {
			if (array_key_exists($lang, $translations[$string])) {
				echo $translations[$string][$lang];
				return;
			}
			//FB::info("No Translation for requested Language $lang found for: '$string'");
		}
	} else {
		FB::warn("No Translation at all found for: '$string'");
	}
	
	echo $string;
}



/**
 * gibt ein var_dump() ordentlich aus
 * über einen pre-HTML-tag
 */
function dump($var) 
{
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}


/**
 * gibt den BasePath aus $_SERVER['php_self'] zurück
 * @example /we/index.php --> /we/
 */
function getBasePath($php_self = null) 
{
	if ($php_self == null)
		$php_self = $_SERVER['PHP_SELF'];
	$path = explode('/', $php_self);
	$scriptname = end($path);
	return str_replace($scriptname, '', $php_self);
}


/**
 * Gibt Blindtext zurück
 */
function getLoremIpsum($words = 50) {
	$loremIpsum = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
	$liArray = explode(" ", $loremIpsum);
	$result = "";
	for ($i=0; $i<$words; $i++) {
		$wordPosition = $i % count($liArray);
		$result = $result . $liArray[$wordPosition] . " ";
	}
	return $result;
}


/**
 * zerlegt einen Pfad aus $_SERVER['REQUEST_URI'] in seine Bestandteile
 * filtert GET-Anhänge aus (alles nach einem ? in der URL)
 */
function getPathInfo($path, $basePath = null)
{
	if (($mark = strpos($path, '?')) !== false) {
		$path = substr($path, 0, $mark);
	}
	
	if ($basePath !== null) {
		$path = substr($path, strlen($basePath));
	}
	
	if ($path == '/' || $path == false)
		return null;
	
	return explode('/', $path);
}

function returnHttp404($message = "Page not found") {
	returnHttpError($message, 404);
}


function returnHttp500($message = "Server Error") {
	returnHttpError($message, 500);
}



/**
 * Gibt eine 404-Fehlermeldung zurück und liest dafür ein Template ein.
 * @see /config/config.php.global --> $config['template']['defaultDirectory']
 */
function returnHttpError($message = "Generic error", $errorcode = 500)
{
	global $config;   // Alternative: Registry-Pattern
	
	header("HTTP/1.0 $errorcode", false, $errorcode);
	$path = $config['template']['defaultDirectory'].'error'.$errorcode.'.php';
	
	if (file_exists($path)) {
		require $path;
	} else {
		echo $message;
	}
	
	exit();
}

