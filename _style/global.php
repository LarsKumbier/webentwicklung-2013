<?php 
	/**
	 * Diese Datei wird bei allen Dateien geladen und
	 * beherbergt dementsprechend globale Einstellungen,
	 * die f�r alle Seiten und Unterseiten gelten sollen.
	 */
	header('Content-Type: text/css');
	
	$containerWidth = 960;
	$navigationWidth = 120;
	
	## Farbedefinitionen in html/css: #RRGGBB / #RGB
	## rot:  #f00 oder #ff0000
	## blau: #00f oder #0000ff
	## gr�n: #0f0 oder #00ff00
	## pink: #f0f oder #ff00ff
	## ...
	## nur sog. Websafe-Farben: 3er-Schritten
	## #000, #300, #600, #900, ...
	
	$primaryColor = "#f60";
?>
h1 {
	background-color: black;
	color: white;
	font-size: 125%;
	font-weight: bold;
	padding: 3px 5px;
	}

h2 {
	margin-top: 2em;
	font-size: 115%;
	font-weight: bold;
	border-bottom: 1px solid #000;
	}

body {
	background-color: black;
	}

a {
	color: <?php echo $primaryColor; ?>;
	text-decoration: none;
	}

p {
	margin-top: 0.75em;
	}
	
.centered {
	margin-left: auto;
	margin-right: auto;
	}

#content .floatRight {
	float: right;
	margin: 5px 0 5px 5px;
	}

#content .floatLeft {
  float: left;
	margin: 5px 5px 5px 0;
	}
	
#container {
	background-color: white;
	margin-top: 30px;
	width: <?php echo $containerWidth; ?>px;
	}
	
#header {
	padding: 5px 10px;
	width: <?php echo $containerWidth - 2 * 10; ?>px;
	height: 52px;
	border-bottom: 1px solid black;
	}
	
#header img.logo {
	height: 50px;
	float: left; 
	}

#header h1 {
	clear: none;
	display: block;
	background-color: white;
	color: black;
	padding-top: 0;
	padding-left: 70px; 
	padding-bottom: 0;
	font-size: 26px;
	}

#header h2 {
	clear: none;
	display: block;
	background-color: white;
	padding-left: 70px;
	padding-top: 10px;
	margin-top: 0;
	color: black;
	font-size: 16px;
	border-bottom: 0;
	}
	
#navigation {
	clear: left;
	float: left;
	width: <?php echo $navigationWidth; ?>px;
	padding: 10px;
	}
	
#navigation ul li {
	margin-bottom: 1em; 
	}

#content {
	float: left;
	width: <?php echo $containerWidth - $navigationWidth - 2*10 - 2*5; ?>px;
	padding: 5px; 
	}

#message {
	padding: 5px 10px;
	background-color: #f93;
	margin-bottom: 1em;
	}

label {
	clear: left;
	}

input {
	clear: left;
	margin-bottom: 10px;
	}

#footer {
	clear: both;
	color: grey;
	text-align: right;
	background-color: black;
	margin-bottom: 3em;
	}

.footerContent {
	padding-left: 5px;
	padding-right: 5px;
	background-color: white;
	}
	
#footer a {
	color: inherit;
	font-weight: bold;
	}
