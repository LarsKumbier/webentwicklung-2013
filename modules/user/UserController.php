<?php

class UserController {
	public function IndexAction() {
		$this->LoginAction();
	}
	
	
	public function LoginAction() {
		$viewContent = array();
		
		if (array_key_exists('action', $_REQUEST) && $_REQUEST['action'] == 'login') {
			FB::info('Login Action in progress');
			$username = $_REQUEST['username'];
			$password = $_REQUEST['password'];
			
			if (!preg_match('/[a-zA-Z0-9]*/', $username))
				die('Hacking attempt in Username');
			if (!preg_match('/[a-zA-Z0-9\(\)\$\§\!]*/', $password))
				die('Hacking attempt in Password');
			
			$sql = "SELECT role,passhash,firstname,lastname FROM users WHERE login='".mysql_escape_string($username)."'";
			FB::info($sql);
			$result = mysql_query($sql);
			if (mysql_num_rows($result) == 1) {
				FB::info('User found');
				$data = mysql_fetch_assoc($result);
				
				if (Bcrypt::check($password, $data['passhash'])) {
					FB::info('password correct');
					$_SESSION['username'] = $data['username'];
					$_SESSION['userRole'] = $data['role'];
					$_SESSION['userFirstname'] = $data['firstname'];
					$_SESSION['userLastname']  = $data['lastname'];
					
					$_SESSION['msg'] = 'logged you in successfully';
					if (array_key_exists('redirect_to', $_SESSION)) 
					{
						header('Location: '.getBasePath().$_SESSION['redirect_to'], true, 302);
						unset($_SESSION['redirect_to']);
					}
					else 
					{
						header("Location: ".getBasePath(), true, 302);
					}
					exit();
				} else {
					FB::info('Password wrong');
					$_SESSION['msg'] = 'username or password wrong.';
				}
			} else {
				FB::info('User not found');
				$_SESSION['msg'] = 'username or password wrong.';
			}
			
			$viewContent['username'] = @$_REQUEST['username'];
		}
		
		$viewContent['template'] = 'modules/user/views/login.php';
		$viewContent['title'] = 'Please login';
		require 'templates/layout.php';
	}
	
	
	public function LogoutAction() {
		if (array_key_exists('username', $_SESSION)) {
			session_destroy();
			session_start();
			$_SESSION['msg'] = 'you are now logged out';
		} else {
			$_SESSION['msg'] = "can't log you out, dummy - you are not logged in!";
		}
		
		header("Location: ".getBasePath());
		exit();
	}
	
	
	public function OverviewAction() {
		$viewContent['template'] = 'modules/user/views/overview.php';
		
		// Authentifizierung (Linkschutz)
		if (!array_key_exists('username', $_SESSION)) {
			$_SESSION['msg'] = 'protected page - please login first';
			$_SESSION['redirect_to'] = 'user/overview';
			header('Location: '.getBasePath().'user/login');
			exit();
		}
		
		// Autorisierung (Linkschutz)
		if (!array_key_exists('userRole', $_SESSION) ||
		    $_SESSION['userRole'] != 'Admin')
		{
			$_SESSION['msg'] = 'thou shall not pass - admins and wise men with grey beards only';
			header('Location: '.getBasePath());
			exit();
		}
		
		require 'templates/layout.php';
	}
	
}
