				<h1>Please login</h1>
				<form action="<?php echo getBasePath($_SERVER['PHP_SELF']); ?>user/login" method="POST">
					<label for="username">Username:</label>
					<input type="text" 
					       name="username" 
					       id="username" 
					       placeholder="Username"
					       pattern="[a-z]{4,16}"
					       <?php if (empty($viewContent['username'])) echo 'autofocus' ?>
					       value="<?= $viewContent['username'] ?>"><br>
					
					<input type="date" pattern="(\d|[0-2]\d|3[01])\.(\d|0\d|1[0-2])\.(\d{2}|19\d{2}|20\d{2})">
					
					<label for="password">Password:</label>
					<input type="password" 
					       name="password" 
					       id="password" 
					       <?php if (!empty($viewContent['username'])) echo 'autofocus' ?>
					       placeholder="min. 4 Zeichen"><br>
					
					<button type="submit" name="action" value="login">Login</button>
				</form>
