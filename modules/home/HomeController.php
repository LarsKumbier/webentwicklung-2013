<?php

class HomeController {
	public function IndexAction() {
		$viewContent = array();
		$viewContent['template'] = 'modules/home/views/index.php';
		$viewContent['title'] = 'Welcome to my Page';
		$viewContent['curDate'] = date('Y-m-d H:i:s');
		require 'templates/layout.php';
	}
}
