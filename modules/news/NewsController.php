<?php

class NewsController {
	public function IndexAction() 
	{
		$viewContent = array();
		$viewContent['template'] = 'modules/news/views/index.php';
		$viewContent['title'] = 'Neues aus der Welt von Barney';
		$viewContent['curDate'] = date('Y-m-d H:i:s');
		require 'templates/layout.php';
	}
}
